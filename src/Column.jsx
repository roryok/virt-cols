function Column (props) {
    console.log(props.tasks)
    const t = props.tasks;
    return (
        <div className='column'>
            {props.name}
            {t.filter(y => props.rule(y)).map(y => <div className='task'>{y.name}</div>)}
        </div>
    )
}

export default Column;