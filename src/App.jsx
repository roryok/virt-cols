import { useState } from 'react'
import Column from './Column'
import logo from './logo.svg'
import './App.css'

// there are three columns, initially. todo, doing, done
// tasks are set to these by certain criteria

function App() {
  const [count, setCount] = useState(0)

  const columns = [
    // todo: no progress, no subtasks with progress
    { id: 1, name: 'todo', rule: (t) => t.status === 'notstarted' },
    // doing: progress < 100, or any subtasks that are p<100 and any that are p>10  
    { id: 2, name: 'doing', rule: (t) => t.status === 'inprogress' },
    // done: p==100 and no subtasks or subtasks all have p=100
    { id: 3, name: 'done', rule: (t) => t.status === 'done' },
  ]
  
  const tasks = [
    { id: 1, name: 'renovate garage', status: 'notstarted' },
    { id: 2, name: 'watch dune', status: 'inprogress' },
    { id: 3, name: 'leahs birthday', status: 'done' }
  ]

  return (
    <div className="App">
      <header className="App-header">
        <p>Virtual columns. look.</p>
        <div style={{ display: 'flex' }}>
          { columns.map(x => 
            <Column name={x.name} tasks={tasks} rule={x.rule} />
          )}
        </div>
      </header>
    </div>
  )
}

export default App
